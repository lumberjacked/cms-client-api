<?php 

return array(
    'routes' => array(
        'cms-api' => array(
            'type' => 'segment',
            'options' => array(
                'route'    => '/api/:api_key/:controller',
            ),
            'may_terminate' => true,
            'child_routes' => array(
                'api-action' => array(
                    'type'    => 'segment',
                    'options' => array(
                        'route'    => '[/:action][/:id]',
                    ),
                ),

                'api' => array(
                    'type'    => 'segment',
                    'options' => array(
                        'route'    => '[/:id]',
                        'constraints' => array(
                            'id' => '\d+'
                        ),
                    ),
                ),   
            ),
                  
        ),
        
    ),
);