<?php

return array(

    'controllers' => include 'controller.config.php',

    'router' => include 'router.config.php',

    'service_manager' => include 'service.config.php',

    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy'
        )
    ),

    'bas_cms' => array(
        'extensions' => array(
            'api' => array(
            'type'    => 'Cms\Client\Api\Extension\ApiManager',
                'options' => array(
                    'listeners' => array(
                        'api' => 'apiRequester'
                    ),
                )
            )
        )
    )
);
