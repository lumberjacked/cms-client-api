<?php
namespace Cms\Client\Api\Extension;

use Zend\Json\Json;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Stdlib\Parameters;
use Cms\ExtensionManager\Extension\Requester;
use Cms\ExtensionManager\Extension\Responder;
use Cms\ExtensionManager\Extension\ResponderEvent;
use Cms\ExtensionManager\Extension\AbstractExtension;

class ApiManager extends AbstractExtension {

    protected $api_exceptions = array(
            'installation'
        );

    public function apiRequester(ResponderEvent $e) {
    	
        if(!($e->getParams() instanceof Requester)) {
            return $e->responder(null, true, 
                         sprintf("Api calls require the use of Cms/ExtensionManager/Extension/Requester -- %s given instead", 
                             gettype($e->getParams())
                        ),
                        $e->getParams(),
                        500
            );
        }

        $requester = $e->getParams();
        
        return $requester->request();
    }

    

    protected function hydrateResponse(Response $response, Responder $responder) {
        
        $response = Json::decode($response->getContent(), Json::TYPE_ARRAY);
        
        if(array_key_exists('name', $response)) {
            $responder->setName($response['name']);
            
            return $responder->response($response['is_error'], $response['message'], $response['data'], $response['status_code']);   
        
        } else {
            $responder->setName('api-requester');
            return $responder->response(false, 'api called successfully', $response);
        }



        
    }	
}