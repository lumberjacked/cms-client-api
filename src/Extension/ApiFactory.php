<?php
namespace Cms\Client\Api\Extension;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ApiFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
    	
        return new ApiManager();
    }
}